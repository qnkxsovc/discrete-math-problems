---
layout: post
title:  "Binomial Trees Identity"
date:   2019-09-12 19:22:14 -0400
tags: [induction, combinatorics, pascal]
---

Prove the following identity: $$\sum_{i=0}^n {n \choose i} = 2^n$$ This is necessary to prove the efficiency of Binomial and Fibonnaci Heap datastructures.


Proof:

We prove by induction on $n$. That is, we assume a proposition $P(n)$, that the statement is true for a *particular* $n$:

$$P(n)\text{:} \sum^n_{i=0} {n \choose i} = 2^n$$

We then use this assumption to prove $P(n+1)$. The first obstacle is that every term in the sum $\sum_{i=0}^{n+1} {n+1 \choose i}$ is different from its counterpart ${n \choose i}$ in $P(n)$, which prevents us from applying the induction hypothesis.

To fix that, we need an identity relating ${n+1 \choose i}$ to ${n \choose i}$. We can pull this from Pascal's triangle:

<img src="{{ site.url }}/images/Pascal.svg" />

In particular, we know that each item in the triangle is the sum of its upper parent (the item directly above it in the triangle) and its upper-left parent.

We know that Pascal's Triangle <a href="https://www.cliffsnotes.com/study-guides/algebra/algebra-ii/additional-topics/binomial-coefficients-and-the-binomial-theorem">has all the binomial coefficients</a>, and we know how to construct row $n+1$ by summing pairs of items from row $n$. We can view the induction hypothesis as the statement that the *sum* of row $n$ of Pascal's triangle is $2^n$.

Mathematically, we have the identity

$${n+1 \choose i} = {n \choose i-1} + {n \choose i}$$

which we can then use to break apart the $P(n+1)$ sum:

$$ \sum_{i=0}^{n+1} {n+1 \choose i} = \sum_{i=0}^n {n \choose i} + \sum_{i=1}^{n+1} {n \choose i-1}$$

Note that the indices of the right hand sum are different from each other. This is because the identity works a bit differently for items at the *ends* of a row, which have only one parent. The right hand sum includes one summation $\sum_{i=0}^{n} {n \choose i}$ over all *upper parents*, for those items that have them, and the other summation $\sum_{i=1}^{n+1} {n+1 \choose i-1}$, which sums the *upper-left parents* from those items that have them.

So, we now have a summation of row $n+1$, defined as two summations of items from row $n$, and derived from the parent-child relationship in Pascal's Triangle.

Now, every item in row $n$ is both an upper parent and an upper left parent for some item in row $n+1$. It is worth verifying this in the Triangle above, and it means that *that the sum over upper parents is the same as the sum over upper-left parents*. Both are just sums over row $n$. 

Finally, we can apply the induction hypothesis: $\sum^n_{i=0} {n \choose i} = 2^n$. In terms of Pascal's Triangle, this tells us that the sum over items in row $n$ is $2^n$. Since the sum over row $n+1$ is twice this value, we see that $P(n+1)$ is true:

$$\sum^{n+1}_{i=0} {n+1 \choose i} = 2(2^n) = 2^{n+1}$$. 

Thus we have proved the induction hypothesis. I want to stress the intuition behind this statement: *row $n+1$ is double row $n$, because every item in row $n$ appears in row $n+1$ once as an upper parent, and once as an upper-left parents*. Since the very first row starts at $1$, the base case $2^0 = 1$ works out, which is the last step in the proof.
