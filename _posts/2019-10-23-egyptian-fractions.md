---
layout: post
title:  "Egyptian Fractions"
date:   2019-09-12 19:22:14 -0400
tags: [integers]
---

Show that any rational number $\frac{p}{q}$ may be expressed as a sum of finitely many *unique* unit fractions (a unit fraction is the inverse of an integer). That is, $\frac{p}{q} = \sum_{i=0}^N \frac{1}{a_i}$ for unique integers $a_i$.


Proof:

We will propose a greedy algorithm for finding a solution, and show that it must terminate in finitely many steps. This algorithm proceeds in rounds: $r=1, 2, 3...$ In each round, the algorithm updates the current target value $\frac{p_i}{q_i}$ with a smaller value. First, choose the integer $a_i$ so that $\frac{1}{a_i}$ is the largest unit fraction that is less than $\frac{p_i}{q_i}$. Then, update $\frac{p_{i+1}}{q_{i+1}} \gets \frac{p_i}{q_i} - \frac{1}{a_i}$.

Now, we want to show that our choice $\frac{1}{a_i}$ is unique, and that this procedure must eventually terminate. One way we can do this is to show that $p_{i+1} < p_i$ and $q_{i+1} > q_i$ always. This would guarantee termination, since $p_i$ is a finite integer, and so our sequence $p_1, p_2, p_3, ...$ can't be decreasing forever. I will show after proving this statement how it also guarantees uniqueness. The idea to prove this particular statement came from inspecting test cases of the algorithm, and the mechanics used to prove it also happen to guarantee uniqueness, which I did not know at the start.

Inspecting our update operation, we see that 

$
\begin{align}
\frac{p_i}{q_i} - \frac{1}{a_i} = \frac{p_i a_i - q_i}{q_i a_i}
\end{align}
$

It must be that $a_i > 1$, so $q_{i+1} = {q_i a_i} > q_i$. But we must also show that $p_i a_i - q_i < p_i$. This is a bit less obvious, but an easy way to determine how to prove this is to observe that it *isn't* true for an arbitrary $a_i$. If $a_i$ were arbitrarily huge, then $p_i a_i - q_i$ could also be arbitrarily huge, so we have to find out what is special about $a_i$ in this case.

By inspection, it's easy to show that $a_i = \lfloor \frac{q_i}{p_i}$. We can write this instead as $a_i = \frac{q_i + k}{p_i}$, where $0 \leq k < p$. This is exactly the identity we need:

$
\begin{align}
p_i a_i - q_i = p_i ( \frac{q_i + k}{p_i} ) - q_i = k < p_i
\end{align}
$

which guarantees that $p_{i+1} < p_i$.

We can also use this fact to show that $a_i$ is unique: since $p_i$ is getting smaller and $q_i$ is getting bigger, $\lfloor \frac {q_i} {p_i} \rfloor = a_i$ must also be getting bigger. (One must show specifically that since $q_i$ and $p_i$ change by at least $\pm 1$, $a_i$ also increases by at least $1$, but this is not hard).