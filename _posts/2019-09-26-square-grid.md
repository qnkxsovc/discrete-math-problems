---
layout: post
title:  "Filling a Square Grid"
date:   2019-09-12 19:22:14 -0400
tags: [counting]
---

Consider a square grid. When the sides have even length, one can exactly fill the grid using rectangular tiles, which are the size of two grid cells joined along a shared edge. By "exactly filling," we mean that no tile sticks out past the outside edge of the square grid.

For example, you could exactly fill an $8x8$ grid with rectangles like so:
<br />
<img src="{{ site.url }}/images/Dominoes/Full_Rectangle.svg" />

Prove or disprove the following: when the square grid has two cells at opposing corners removed, one can exactly fill the grid with rectangular tiles.
<br />
<img src="{{ site.url }}/images/Dominoes/Corners_Blocked.svg" />

Proof:

The statement is not true. That is, blocking off the corners makes it impossible to fill the square grid with rectangular tiles. 
Let $s$ be the side length, and let $N=s^2$ be the number of cells in the square grid. Blocking the corners leaves $N-2$ tiles to cover.

Assign dots to alterntaing cells, so that both of the blocked corners are dot-less. 
<br />
<img src="{{ site.url }}/images/Dominoes/Corners_Blocked_Dots.svg" />

If the square grid is covered in rectangular tiles, then each dotted cell has one half of a rectangular tile on top of it. None of the rectangular tiles can cover two dots at once, because the other half of any tile on a dotted cell must lie on top of one of the cardinal neighbors of the dotted cell. None of these cardinal neighbors have dots.

This means we have *at least* as many rectanglular tiles as dotted cells. There are exactly $N/2$ dotted cells, so we have to have at least $\frac{N}{2}$ rectangles. But each rectangle covers $2$ cells in the square grid, so overall they cover $(2)(\frac{N}{2}) = N$ grid cells, even though there are only $N-2$ grid cells available. So it is impossible to exactly fill the grid.